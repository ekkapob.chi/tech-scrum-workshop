const count = require('./count').count;

test('find "A" in "" returns 0', () => {
  expect(count('', 'abc')).toBe(0);
});
